/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package martinez.nieto.pol.m06uf2ex1;

import com.github.javafaker.Faker;
import entitats.Stock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Pol Nieto Martínez
 */
public class M06UF2EX1 {

    private static SessionFactory factory;
    private static Session session;
    private static final Logger logger = LogManager.getLogger(M06UF2EX1.class);

    public static void main(String[] args) {
        try {
            factory = new Configuration().configure("hibernateConfig/hibernate.cfg.xml").buildSessionFactory();

            logger.trace("Iniciem sessió...");
            session = factory.openSession();

            logger.trace("Iniciem transaccio...");
            Transaction tx = session.beginTransaction();

            logger.trace("Creem objectes");
            Faker faker = new Faker();

            for (int i = 0; i < 1000; i++) {
                Stock stock = new Stock();
                stock.setStockCode(faker.code().ean13());
                stock.setStockName(faker.commerce().productName());
                session.save(stock);
            }

            logger.info("Finalitzem transacció i desem a BBDD...");
            tx.commit();

        } catch (ConstraintViolationException ex) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            logger.error("Violació de la restricció: " + ex.getMessage());

        } catch (HibernateException ex) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }

            logger.error("Excepció d'hibernate: " + ex.getMessage());
        } catch (Exception ex) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            logger.error("Excepció: " + ex.getMessage());
        } finally {
            session.close();
            factory.close();
        }

    }
}
